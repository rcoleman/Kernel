/*
 * Copyright (c) 2021, RISC OS Open Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of RISC OS Open Ltd nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "modhead.h"
#include "swis.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include "Global/RISCOS.h"
#include "Global/Services.h"
#include "Global/HALDevice.h"
#include "Global/HALEntries.h"
#include "Global/NewErrors.h"

#ifdef USE_SYNCLIB
#include "SyncLib/synclib.h"
#endif

#include "../../../aborttrap/aborttrap.h"
#include "../../../aborttrap/aterrors.h"
#include "../../../kerneliface.h"

extern void veneer(void);
extern void veneer_config(void *pw,int old_handler);
static int old_handler;

void *kalloc(size_t size,_kernel_oserror **e)
{
	/* A generic error should be sufficient */
	static _kernel_oserror err = {0,"malloc failed"};
	void *p = malloc(size);
	if (!p)
		*e = &err;
	return p;
}

_kernel_oserror const ErrorBlock_BadParameters = {ErrorNumber_BadParameters,"Bad parameters"};

_kernel_oserror *TranslateError(_kernel_oserror const *e)
{
	/* We don't care about translating errors */
	return (_kernel_oserror *) e;
}

_kernel_oserror* module_init(const char *cmd_tail, int podule_base, void *pw)
{
	_kernel_oserror* e = 0;

#ifdef USE_SYNCLIB
	synclib_init();
#endif

	aborttrap_init();

	/* Register handler */
	_kernel_irqs_off();
	e = _swix(OS_ClaimProcessorVector,_INR(0,1)|_OUT(1),0x104,veneer,&old_handler);
	if (!e)
		veneer_config(*((void **)pw),old_handler);
	_kernel_irqs_on();
	if (e)
		goto error;
    
	return 0;

error:
	return e;
}

_kernel_oserror *module_final(int fatal, int podule, void *pw)
{
	_kernel_oserror* e = 0;

	e = _swix(OS_ClaimProcessorVector, _INR(0,2), 0x4,old_handler,veneer);
	if(e)
		return e;

	return NULL;
}

#ifdef ABORTTRAP_DEBUG
logentry logbuf[LOG_SIZE];
uint32_t logidx;

_kernel_oserror *module_commands(const char *arg_string, int argc, int cmd_no, void *pw)
{
	switch (cmd_no) {
	case CMD_AbortTrapLog:
		{
		uint32_t base = logidx;
		printf("Address   Opcode    PSR       Result      Disassembly/location\n");
		for(uint32_t offset=0;offset<LOG_SIZE;offset++)
		{
			logentry *l = &logbuf[(base+offset)&(LOG_SIZE-1)];
			if(l->pc != 0)
			{
				/* Disassemble */
				printf("%08X: %08X: %08X: ",l->pc,l->opcode,l->psr);
				switch((int)l->result)
				{
				case (int) aborttrap_ERROR_UNHANDLED: printf("UNHANDLED  "); break;
				default: printf("%08x   ",l->result->errnum); break;
				case 0: printf("CONTINUE   "); break;
				}
				char *temp=0;
				switch(l->psr & PSR_ISET)
				{
				case PSR_ISET_ARM:
					_swix(Debugger_Disassemble,_INR(0,1)|_OUT(1),l->opcode,l->pc,&temp);
					break;
				case PSR_ISET_THUMB:
				case PSR_ISET_THUMBEE: /* Not good! */
					_swix(Debugger_DisassembleThumb,_INR(0,1)|_OUT(1),l->opcode,l->pc,&temp);
					break;
				}
				if(temp)
				{
					int len = strlen(temp);
					puts(temp);
					do {
						putchar(' ');
					} while(len++<40);
				}
				/* Work out where it was */
				if((l->pc >= 0x8000) && (l->pc < 512<<20))
				{
					printf("Application space");
				}
				else
				{
					/* Check modules */
					int mod=0,inst=0;
					uint32_t base;
					uint32_t bestbase=0;
					while(!_swix(OS_Module,_INR(0,2)|_OUTR(1,3),12,mod,inst,&mod,&inst,&base))
					{
						if(!inst)
						{
							if(l->pc-base <= ((uint32_t *)bestbase)[-1])
								bestbase = base;
						}
					}
					if(bestbase)
					{
						printf("Module '%s' (%08X offset %08X)\n",(char *)(bestbase+((uint32_t *)bestbase)[4]),bestbase,l->pc-bestbase);
					}
					else
					{
						printf("Unknown location\n");
					}
				}
			}
		}
		memset(logbuf,0,sizeof(logbuf));
		}
		break;
	case CMD_AbortTrapHandlers:
		aborttrap_list_handlers();
		break;
	}
	return 0;
}
#endif

_kernel_oserror *module_swihandler(int swi_offset, _kernel_swi_regs *r, void *pw)
{
	switch (swi_offset)
	{
	case AbortTrap_AbortTrap-AbortTrap_00:
		return aborttrap_swi(r->r[0],r->r[1],r->r[2],r->r[3],r->r[4]);
	default:
		return error_BAD_SWI;
	}
}
